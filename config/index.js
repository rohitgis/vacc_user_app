export const GRAY = '#a6adbb';
export const BLACK = '#20232a';
export const THEME_COLOR = "#ff9e1b";
export const THEME_COLOR_LIGHT = "#fff1de";
export const BLUE = "#2196f3";
export const LIGHT_GRAY = "#f9e4e2";
export const DARK_GRAY = "#a5acb9";
export const SUCCESS_COLOR = "#8dc73d";

export const API_URL = "http://2.56.215.239:3009/api";