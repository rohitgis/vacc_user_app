import { AsyncStorage } from "react-native";
import { API_URL } from "_config";

export const getNearByHospitals = async () => {
    const latitude = "23.6119";
    const longitude = "23.6119";
    try {
        const response = await fetch(`${API_URL}/booking/nearby?long=${longitude}&latt=${latitude}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result.success === true) {
                return result.data;
            }
        }
        return [];
    } catch (error) {
        console.log(error)
    }
}

export const getAvailableDate = async (id, date) => {
    try {
        const response = await fetch(`${API_URL}/availability/get/date?vaccCenter=${id}&startDate=${date}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result.success === true) {
                return result.data;
            }
        }
        return [];
    } catch (error) {
        console.log(error)
    }
}