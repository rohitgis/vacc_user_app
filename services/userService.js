import { AsyncStorage } from "react-native";
import { API_URL } from "_config";

export const createUser = async (govtId, mobileNumber, idType, name, email, dob, address, city, district, state, postBox, doses) => {
    const data = {
        govtId,
        mobileNumber,
        idType,
        name,
        email,
        dob,
        address,
        city,
        district,
        state,
        postBox,
        doses
    }
    console.log("idNumber",govtId)
    console.log("mobileNo",mobileNumber)
    console.log("idType",idType)
    console.log("name",name)
    console.log("email",email)
    console.log("dob",dob)
    console.log("address",address)
    console.log("city",city)
    console.log("district",district)
    console.log("state",state)
    console.log("postBox",postBox)
    console.log("doses",doses)
    try {
        let response = await fetch(`${API_URL}/user`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        console.log(response)
        if (response.status === 200) {
            let result = await response.json();
            return result;
        }
        return []
    } catch (error) {
        console.log(error)
    }
}

export const userCheck = async (mobileNumber, govtId) => {
    const data = {
        mobileNumber,
        govtId
    }
    try {
        let response = await fetch(`${API_URL}/user/checking`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            return result;
        }
        return [];
    } catch (error) {
        console.log(error)
    }
}

export const getUserBookingDetail = async () => {
    const userId = await AsyncStorage.getItem('userId');
    console.log(userId, "uid")
    try {
        const response = await fetch(`${API_URL}/booking/user/${userId}`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json'
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result.success === true) {
                return result.data;
            }
        }
        return [];
    } catch (error) {
        console.log(error)
    }
}