import React, { useState, useEffect } from "react";
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import { THEME_COLOR, DARK_GRAY } from "_config";

const markingType = { "selected": true, "selectedColor": THEME_COLOR, textColor: THEME_COLOR };
const disableMarking = { disabled: true, disableTouchEvent: true };

const CalendarView = (props) => {
    const { handleMonthChange, loader, availableDates, nextDay, setNextDay } = props;
    const todayDate = new Date().toISOString().slice(0, 10);

    const [markedDate, setMarkedDate] = useState({})
    // const [nextDay, setNextDay] = useState([]);
    const [daysInMonth, setDaysInMonth] = useState(0);
    const [currentMonthYear, SetCurrentMonthYear] = useState("");
    const [modifiedAvailableDates, setModifiedAvailableDates] = useState([]);
    const [disabledDate, setDisableDate] = useState([]);

    const handleDayPress = (date) => {
        setNextDay([date])
    }
    useEffect(() => {
        let dates = availableDates.map((el, index) => {
            return el.trDate.slice(0, 10)
        })
        setModifiedAvailableDates(dates)

    }, [])
    useEffect(() => {
        let newDaysObject = {};
        nextDay.forEach((day) => {
            newDaysObject[day] = markingType;
        });
        setMarkedDate(newDaysObject)
    }, [nextDay])

    // const getDaysInMonth = (month, year) => {
    //     return new Date(year, month, 0).getDate();
    // }


    useEffect(() => {
        var array = []
        if (currentMonthYear && modifiedAvailableDates.length > 0) {
            for (let i = 1; i <= daysInMonth; i++) {

                if (String(i).length < 2) {
                    var currDate = currentMonthYear.concat('-0').concat(i)
                } else {
                    var currDate = currentMonthYear.concat('-').concat(i)
                }
                if (!modifiedAvailableDates.includes(currDate)) {
                    array.push(currDate)
                }
            }
            console.log(array, "jhbchsb")
            setDisableDate(array)
        }
    }, [modifiedAvailableDates, currentMonthYear, daysInMonth])
    useEffect(() => {
        let newDaysObject = { ...markedDate };
        disabledDate.forEach((day) => {
            newDaysObject[day] = disableMarking;
        });
        setMarkedDate(newDaysObject)
    }, [disabledDate])
    const handleVisibleMonthChange = async (month) => {
        setDaysInMonth(new Date(month[0].year, month[0].month, 0).getDate());
        SetCurrentMonthYear(month[0].dateString.slice(0, 7))
        handleMonthChange(month[0].dateString)
    }
    return (

        <CalendarList
            // testID={testIDs.horizontalList.CONTAINER}
            // markingType={'period'}
            onVisibleMonthsChange={(month) => {
                handleVisibleMonthChange(month)

            }}
            markedDates={markedDate}
            current={todayDate}
            minDate={todayDate}
            pastScrollRange={0}
            futureScrollRange={4}
            horizontal
            pagingEnabled
            disableAllTouchEventsForDisabledDays={true}
            displayLoadingIndicator={loader}
            onDayPress={(day) => handleDayPress(day.dateString)}
            // showScrollIndicator={true}
            theme={{
                backgroundColor: '#ffffff',
                calendarBackground: '#ffffff',
                textSectionTitleColor: '#b6c1cd',
                textSectionTitleDisabledColor: '#d9e1e8',
                selectedDayBackgroundColor: '#00adf5',
                selectedDayTextColor: '#ffffff',
                todayTextColor: THEME_COLOR,
                dayTextColor: '#2d4150',
                textDisabledColor: DARK_GRAY,
                dotColor: '#00adf5',
                selectedDotColor: '#ffffff',
                arrowColor: 'orange',
                disabledArrowColor: '#d9e1e8',
                monthTextColor: THEME_COLOR,
                indicatorColor: THEME_COLOR,
                textDayFontFamily: 'monospace',
                textMonthFontFamily: 'monospace',
                textDayHeaderFontFamily: 'monospace',
                textDayFontWeight: '300',
                textMonthFontWeight: 'bold',
                textDayHeaderFontWeight: '300',
                textDayFontSize: 16,
                textMonthFontSize: 16,
                textDayHeaderFontSize: 16
            }}
        />
    )
}
export default CalendarView;