import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Platform, TouchableOpacity, PermissionsAndroid, ActivityIndicator } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import { THEME_COLOR, WHITE } from '_config';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const MapLocation = (props) => {
    const { setLatitude, setLongitude, RBSheetRef } = props;
    const [location, setLocation] = useState(null);
    const [markerLocation, changeMarkerLocation] = useState(null);
    const [region, setRegion] = useState(null);
    var tempRegion;
    useEffect(() => {
        requestLocationPermission();
        return () => {
            setLocation(null);
        };
    }, []);
    const requestLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            getOneTimeLocation();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: 'Location Access Required',
                        message: 'This App needs to Access your location',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    getOneTimeLocation();
                } else {
                    console.log('Permission Denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };
    const getOneTimeLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                // setRegion(position.coords)
                // setLatitude(String(position.coords.latitude))
                // setLongitude(String(position.coords.longitude))
                // console.log(position.coords)
                // console.log(position.coords.latitude)
                tempRegion = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                };
                const geometry = {
                    location: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }
                }
                setRegion(tempRegion);
                setMap(geometry);
            },
            (error) => {
                console.log(error);
                console.log("error");
            },
            {
                enableHighAccuracy: true,
                timeout: 30000,
                maximumAge: 1000
            },
        );
    };
    const setMap = async (geometry) => {
        try {
            //   if (Platform.OS === 'android' && !Constants.isDevice) {
            //     console.log(
            //       'Oops, this will not work on Sketch in an Android emulator. Try it on your device!'
            //     );
            //   } else {
            // let { status } = await Location.requestPermissionsAsync();
            // if (status !== 'granted') {
            //   console.log('Permission to access location was denied');
            // }
            //   }
            setLocation(geometry.location);
            // updateMap(geometry.location);
        } catch (error) {
            console.log('error in update map');
        }
    };
    const updateMap = async (location) => {
        try {
            setRegion(tempRegion);
            changeMarkerLocation(location);
            // let locationObj = {
            //     latitude: location.lat,
            //     longitude: location.lng,
            // };
            // let locationPlace = await Location.reverseGeocodeAsync(locationObj);

            // if (locationPlace) {
            //     setFullAddress(locationPlace[0]);
            // }
        } catch (error) {
            console.log('error', error);
        }
    };
    const handleConfirmLocation = () => {
        if (markerLocation) {
            setLatitude(String(markerLocation.latitude))
            setLongitude(String(markerLocation.longitude))
            RBSheetRef.current.close();
            // props.navigation.navigate("BranchDetail")
        } else {
            alert("Please select any location")
        }
    }
    return (
        <View style={styles.container}>
            {location ? <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                onPress={(e) => {
                    console.log(e.nativeEvent)
                    updateMap(e.nativeEvent.coordinate);
                }}
                showsUserLocation={true}
                region={region}
                onRegionChange={(e) => {
                    tempRegion = e;
                }}
            >
                {markerLocation && <Marker
                    coordinate={markerLocation}
                // title={"JavaTpoint"}
                // description={"Java Training Institute"}
                />}
            </MapView> : <View>
                <ActivityIndicator size="large" color={THEME_COLOR} />
                <Text>Loading Map...</Text>
            </View>}
            <TouchableOpacity activeOpacity={0.7} style={styles.cancelButton}
                onPress={() => {
                    RBSheetRef.current.close()
                }}>
                <Text style={styles.confirmText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.7} style={styles.ConfirmButton}
                onPress={handleConfirmLocation}>
                <Text style={styles.confirmText}>Confirm</Text>
            </TouchableOpacity>
        </View>
    )
}

export default MapLocation;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        // width: width,
        // height: height
    },
    arrowLeftStyle: {
        height: 20,
        width: 20,
    },
    backButtonContainer: {
        width: 45,
        height: 45,
        margin: 20,
        borderRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: WHITE
    },
    ConfirmButton: {
        width: 130,
        paddingVertical: 10,
        borderRadius: 30,
        backgroundColor: THEME_COLOR,
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        bottom: 15,
        right: 10
    },
    cancelButton: {
        width: 130,
        paddingVertical: 10,
        borderRadius: 30,
        backgroundColor: THEME_COLOR,
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        bottom: 15,
        left: 10
    },
    confirmText: {
        color: "#fff",
        fontSize: 16,
    }
})