import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, ScrollView } from 'react-native';
import { THEME_COLOR } from '_config';
import { HeaderBackButton } from '_components';
import { listCalendars, addCalendarEvent } from './calendar/calendarEvent';

const { width } = Dimensions.get("window");

const PreConfirmation = (props) => {
    const { userData, hospital, date, bookingTime } = props.route.params;
    const [calendar, setCalendar] = useState([]);

    useEffect(() => {
        localCalendars();
    }, [])
    const handleConfirmBooking = () => {
        saveEvent();
        props.navigation.replace("Booked")
    }
    const saveEvent = async () => {
        var hour = 10;
        var min = 10;

        var dateString = new Date(`${date[0]} ${hour}:${min}`)
        hour = dateString.getHours() - 11;
        min = dateString.getMinutes()

        var newDateString = new Date(`${date[0]} ${hour}:${min}`)
        var newEndDateString = new Date(`${date[0]} ${hour}:${min + 10}`)
        const event = {
            title: "Vaccination Appointment",
            startDate: newDateString.toISOString(),
            endDate: newEndDateString.toISOString(),
            allDay: false,
            description: "Today is your vaccination Appointment is scheduled. Be on time. All the best",
            alarms: [{
                date: 180
            }]
        }
        await addCalendarEvent(event, calendar[0]);
    }
    const localCalendars = async () => {
        try {
            let calendars = await listCalendars();
            var cal = calendars.filter((item) => item.allowsModifications && item.isPrimary)
            setCalendar([cal[0]])
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <View style={styles.container}>
            <HeaderBackButton {...props} name="Confirm Details" />
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollContainer}>
                <View style={styles.singleRow}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Name</Text>
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.normalText}>{userData.name}</Text>
                    </View>
                </View>
                {/* <View style={styles.singleRow}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Relation</Text>
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.normalText}>{userData.rel}</Text>
                    </View>
                </View> */}
                <View style={styles.singleRow}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Center Detail</Text>
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.normalText}>{hospital.name}</Text>
                        <Text style={styles.normalText}>{hospital.address}</Text>
                        <Text style={styles.normalText}>{hospital.city}</Text>
                    </View>
                </View>
                <View style={styles.singleRow}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Date</Text>
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.normalText}>{date[0]}</Text>
                    </View>
                </View>
                <View style={styles.singleRow}>
                    <View style={styles.headingContainer}>
                        <Text style={styles.heading}>Time</Text>
                    </View>
                    <View style={styles.TextContainer}>
                        <Text style={styles.normalText}>{bookingTime}</Text>
                    </View>
                </View>
                <TouchableOpacity activeOpacity={0.7} style={styles.confirmButton} onPress={handleConfirmBooking}>
                    <Text style={styles.biuttonText}>Confirm & add to calender</Text>
                </TouchableOpacity>
                {/* {calendar.map((item, index) =>
                    item.allowsModifications && item.isPrimary ? (
                        <Text key={index}>{item.title}</Text>
                    ) : null
                )} */}
            </ScrollView>
        </View>
    )
}
export default PreConfirmation;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 15,
        paddingVertical: 20
    },
    headingContainer: {
        width: (width - 30) * 0.3
    },
    singleRow: {
        flexDirection: "row",
        paddingVertical: 5
    },
    heading: {
        fontSize: 16,
        fontWeight: "bold"
    },
    TextContainer: {
        width: (width - 30) * 0.7
    },
    normalText: {
        fontSize: 16,
    },
    confirmButton: {
        backgroundColor: THEME_COLOR,
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 12,
        marginTop: 30,
        borderRadius: 10
    },
    biuttonText: {
        fontSize: 17,
        color: "#fff"
    }
})