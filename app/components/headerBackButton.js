import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { GRAY, BLACK } from "_config";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const HeaderBackButton = (props) => {
    const handleBack = () => {
        props.navigation.goBack()
    }
    return (
        <View style={styles.backContainer}>
           {props.name === "Status" ? <View style={{width: 50}}></View> : <TouchableOpacity
                style={styles.backIcon}
                onPress={handleBack}
                activeOpacity={0.7}>
                <MaterialCommunityIcons name="chevron-left" size={40} color={BLACK} />
            </TouchableOpacity>}
            <Text style={styles.headerName}>{props.name}</Text>
            <View style={{ width: 50 }}></View>
        </View>
    )
}
export default HeaderBackButton;

const styles = StyleSheet.create({
    backContainer: {
        // borderWidth: 1
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    backIcon: {
        // borderWidth: 1,
        padding: 5,
        width: 50
    },
    headerName: {
        fontSize: 18,
        fontWeight: 'bold',
        // paddingTop: 10
    }
})