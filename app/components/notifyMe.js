import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { DARK_GRAY, THEME_COLOR } from '_config';
import CheckBox from '@react-native-community/checkbox';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import RBSheet from "react-native-raw-bottom-sheet";
import { MapLocation } from '_components';

const { height } = Dimensions.get("window");

const NotifyMe = (props) => {
    const { refRBSheet } = props;
    const RBSheetRef = useRef();
    const [sunCheck, setSunCheck] = useState(false);
    const [monCheck, setMonCheck] = useState(false);
    const [tueCheck, setTueCheck] = useState(false);
    const [wedCheck, setWedCheck] = useState(false);
    const [thuCheck, setThuCheck] = useState(false);
    const [friCheck, setFriCheck] = useState(false);
    const [satCheck, setSatCheck] = useState(false);
    const [latitude, setLatitude] = useState("");
    const [longitude, setLongitude] = useState("");
    return (
        <View style={styles.conatiner}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={styles.heading}>Select Notifying Days</Text>
                <View style={styles.dayBoxContainer}>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={sunCheck}
                            onValueChange={(value) => setSunCheck(value)}
                        />
                        <Text style={styles.normalText}>Sun</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={monCheck}
                            onValueChange={(value) => setMonCheck(value)}
                        />
                        <Text style={styles.normalText}>Mon</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={tueCheck}
                            onValueChange={(value) => setTueCheck(value)}
                        />
                        <Text style={styles.normalText}>Tue</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={wedCheck}
                            onValueChange={(value) => setWedCheck(value)}
                        />
                        <Text style={styles.normalText}>Wed</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={thuCheck}
                            onValueChange={(value) => setThuCheck(value)}
                        />
                        <Text style={styles.normalText}>Thu</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={friCheck}
                            onValueChange={(value) => setFriCheck(value)}
                        />
                        <Text style={styles.normalText}>Fri</Text>
                    </View>
                    <View style={styles.singleDay}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={satCheck}
                            onValueChange={(value) => setSatCheck(value)}
                        />
                        <Text style={styles.normalText}>Sat</Text>
                    </View>
                </View>
                <Text style={styles.heading}>Set location to notify according to nearest hospital</Text>
                <TouchableOpacity activeOpacity={0.7} style={styles.locationButton}
                    onPress={() => RBSheetRef.current.open()}
                >
                    <MaterialIcons name="add-location-alt" size={20} style={{ marginRight: 5 }} color={THEME_COLOR} />
                    <Text style={styles.normalText}>Select Your Location</Text>
                </TouchableOpacity>
                {(latitude && longitude) ? <View style={{ marginLeft: 25 }}>
                    <Text style={styles.heading}>{latitude}</Text>
                    <Text style={styles.heading}>{longitude}</Text>
                </View> : <></>}
                <TouchableOpacity activeOpacity={0.7} style={styles.confirmButton} onPress={() => refRBSheet.current.close()}>
                    <Text style={styles.ButtonText}>Confirm</Text>
                </TouchableOpacity>
            </ScrollView>
            <RBSheet
                ref={RBSheetRef}
                closeOnDragDown={false}
                closeOnPressMask={false}
                height={height}
                dragFromTopOnly={true}
                // onClose={() => setToggleCheckBox(false)}
                // onOpen={() => { }}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000038"
                    },
                    draggableIcon: {
                        backgroundColor: THEME_COLOR
                    }
                }}
            >
                <MapLocation
                    {...props}
                    setLatitude={setLatitude}
                    setLongitude={setLongitude}
                    RBSheetRef={RBSheetRef} />
            </RBSheet>
        </View>
    )
}
export default NotifyMe;

const styles = StyleSheet.create({
    conatiner: {
        flex: 1,
        paddingHorizontal: 15
    },
    heading: {
        fontSize: 16,
        color: DARK_GRAY
    },
    singleDay: {
        flexDirection: "row",
        alignItems: "center"
    },
    normalText: {
        fontSize: 16
    },
    dayBoxContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        paddingVertical: 5
    },
    locationButton: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 5
    },
    confirmButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 10,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 10
    },
    ButtonText: {
        fontSize: 16,
        color: "#fff",
    }

})