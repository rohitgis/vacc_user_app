import React, { useEffect } from 'react';
import { View, StyleSheet, LogBox } from 'react-native';
import Application from './app';

const App = () => {
    useEffect(()=>{
        LogBox.ignoreAllLogs();
    },[])
    return <View style={styles.container}>
        <Application />
    </View>
}
export default App;

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})