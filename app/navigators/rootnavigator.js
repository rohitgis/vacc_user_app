import React from 'react';
import { Animated } from 'react-native';
import {
  createStackNavigator
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import LoginStack from "./loginNavigator";
import BottomTab from './bottomTabNavigator';

const HomeStackNavigator = createStackNavigator();

export default ProfileScreenStack = (props) => {
  return (
    <NavigationContainer>
      <HomeStackNavigator.Navigator
        mode="modal"
        initialRouteName="Login"
        
        screenOptions={{
          animationEnabled: true,
          headerShown: false,
          tabBarVisible: false,
          cardStyle: {
            backgroundColor: 'white',
          },
          cardStyleInterpolator: ({ current, next }) => {
            const opacity = Animated.add(
              current.progress,
              next ? next.progress : 0
            ).interpolate({
              inputRange: [0, 1, 2],
              outputRange: [0, 1, 0],
            });

            return {
              leftButtonStyle: { opacity },
              rightButtonStyle: { opacity },
              titleStyle: { opacity },
              backgroundStyle: { opacity },
            };
          },
        }}
        lazy={true}
      >
        <HomeStackNavigator.Screen
          name="Login"
          component={LoginStack}
        />
        <HomeStackNavigator.Screen
          name="Home"
          component={BottomTab}
        />
      </HomeStackNavigator.Navigator>
    </NavigationContainer>
  );
};
