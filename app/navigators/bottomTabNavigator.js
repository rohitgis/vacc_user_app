import React from 'react';
import { View } from "react-native";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import BookedScreen from "_screens/bookedScreen";
import { THEME_COLOR, DARK_GRAY } from "_config";
import StatusNavigator from './statusNavigator';
import HomeNavigator from "./homeNavigator";
import { StartJourney_white_icon, Status_white_icon, Profile_white_icon } from "_icons";

const Tab = createBottomTabNavigator();

const MyTabs = () => {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            backBehavior="initialRoute"

            // screenOptions={{
            //     unmountOnBlur: false,
            // }}
            // lazy={true}
            tabBarOptions={{
                activeTintColor: THEME_COLOR,
                inactiveTintColor: DARK_GRAY,
                labelStyle: { fontSize: 14 },
                style: { paddingTop: 5, height: 60 }
            }}
        >
            <Tab.Screen name="Home" component={HomeNavigator}
                options={({ route }) => {
                    return {
                        tabBarIcon: ({ focused }) => (
                            <View style={focused ? { backgroundColor: THEME_COLOR, padding: 3, borderRadius: 25 } : {}}>
                                <StartJourney_white_icon width={28} height={28} color={focused ? '#fff' : DARK_GRAY} />
                            </View>
                            // <FontAwesome5 name="route" size={28} color={focused ? THEME_COLOR : 'grey'} />
                        )
                    };
                }}
            />
            <Tab.Screen name="Status" component={StatusNavigator}
                options={({ route }) => {
                    return {
                        tabBarIcon: ({ focused }) => (
                            <View style={focused ? { backgroundColor: THEME_COLOR, padding: 3, borderRadius: 25 } : {}}>
                                <Status_white_icon width={28} height={28} color={focused ? "#fff" : DARK_GRAY} />
                            </View>
                        ),
                    };
                }}
            />
            <Tab.Screen name="Profile" component={StatusNavigator}
                options={({ route }) => {
                    return {
                        tabBarIcon: ({ focused }) => (
                            <View style={focused ? { backgroundColor: THEME_COLOR, padding: 3, borderRadius: 25 } : {}}>
                                <Profile_white_icon width={28} height={28} color={focused ? '#fff' : DARK_GRAY} />
                            </View>
                        ),
                    };
                }}
            />
        </Tab.Navigator>
    );
}
export default MyTabs;