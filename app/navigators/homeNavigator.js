import React from 'react';
import Animated from 'react-native-reanimated';
import {
    createStackNavigator,
    CardStyleInterpolators,
} from '@react-navigation/stack';
import { Easing } from 'react-native-reanimated';

import BookedScreen from "_screens/bookedScreen";
import HomeScreen from '_screens/home';
import AppointmentBook from '_screens/appointment';
import HospitalList from "_screens/hospitalList";
import { PreConfirmation } from '_components';

const HomeStackNavigator = createStackNavigator();
const forFade = ({ current, next }) => {
    const opacity = Animated.add(
        current.progress,
        next ? next.progress : 0
    ).interpolate({
        inputRange: [0, 1, 2],
        outputRange: [0, 1, 0],
    });

    return {
        leftButtonStyle: { opacity },
        rightButtonStyle: { opacity },
        titleStyle: { opacity },
        backgroundStyle: { opacity },
        cardStyle: { opacity },
        // overlayStyle: { opacity },
    };
};
const config = {
    // animation: 'spring',
    // config: {
    //     stiffness: 1000,
    //     damping: 50,
    //     mass: 3,
    //     overshootClamping: false,
    //     restDisplacementThreshold: 0.01,
    //     restSpeedThreshold: 0.01,
    // },
    animation: 'timing',
    config: {
        duration: 200,
        easing: Easing.linear,
    },
};

const closeConfig = {
    animation: 'timing',
    config: {
        duration: 200,
        easing: Easing.linear,
    },
};

export default ProfileScreenStack = (props) => {
    return (
        // <NavigationContainer>
        <HomeStackNavigator.Navigator
            mode="modal"
            initialRouteName="Home"
            animationEnabled={true}
            animation="slide"
            screenOptions={{
                headerShown: false,
                // gestureEnabled: true,
                // gestureDirection: 'horizontal',
                animationEnabled: true,
                cardStyle: {
                    backgroundColor: 'white',
                },
                cardStyleInterpolator: (e) => {
                    const { current, next } = e;
                    let customConfig = CardStyleInterpolators.forHorizontalIOS(e);
                    return customConfig;
                },
                transitionSpec: {
                    open: config,
                    close: closeConfig,
                },
            }}
            lazy={true}
        >
            <HomeStackNavigator.Screen
                name="Home"
                component={HomeScreen}
            />
            <HomeStackNavigator.Screen
                name="appointmentBook"
                component={AppointmentBook}
            />
            <HomeStackNavigator.Screen
                name="Booked"
                component={BookedScreen}
            />
            <HomeStackNavigator.Screen
                name="PreConfirmation"
                component={PreConfirmation}
            />
            <HomeStackNavigator.Screen
                name="HospitalList"
                component={HospitalList}
            />
        </HomeStackNavigator.Navigator>
    );
};
