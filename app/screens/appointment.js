import React, { useEffect, useState, useRef } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, TextInput, Dimensions, TouchableOpacity, Picker } from 'react-native';
import { THEME_COLOR, DARK_GRAY, BLACK } from "_config";
import { HeaderBackButton, NotifyMe } from "_components";
import { CalendarView } from "_components";
import { getAvailableDate } from "_services/hospitalService";
import CheckBox from '@react-native-community/checkbox';
import RBSheet from "react-native-raw-bottom-sheet";

const { width, height } = Dimensions.get('window');
const timeSlot = [
    {
        time: '10:00 AM'
    },
    {
        time: '10:10 AM'
    },
    {
        time: '10:20 AM'
    },
    {
        time: '10:30 AM'
    },
    {
        time: '10:40 AM'
    },
    {
        time: '10:50 AM'
    },
]

const AppointmentBook = (props) => {
    const { userData, hospital } = props.route.params;
    const refRBSheet = useRef();

    const todayDate = new Date().toISOString().slice(0, 10);
    const [selectedValue, setSelectedValue] = useState('');
    const [activeSlot, setActiveSlot] = useState(null);
    const [bookingTime, setBookingTime] = useState(null)
    const [startDate, setStartDate] = useState(todayDate);
    const [loader, setLoader] = useState(false);
    const [availableDates, setAvailableDates] = useState([]);
    const [nextDay, setNextDay] = useState([]);
    const [toggleCheckBox, setToggleCheckBox] = useState(false);

    // console.log(item, "hospital details...")
    useEffect(() => {
        if (toggleCheckBox === true) {
            refRBSheet.current.open()
        }
    }, [toggleCheckBox])
    const handleTimeSlot = (item, index) => {
        setActiveSlot(index)
        setBookingTime(item.time)
    }
    useEffect(() => {
        getAvailableDateList();
    }, [])
    const getAvailableDateList = async () => {
        setLoader(true)
        // let res = await getAvailableDate(item._id, startDate)
        // if (res.length > 0) {
        //     setAvailableDates(res)
        //     console.log(res, "available dates..")
        // }
        setLoader(false)
    }
    const handleMonthChange = (date) => {
        setStartDate(date)
    }
    return (
        <View style={styles.container}>
            <HeaderBackButton {...props} name="Book Appointment" />
            <ScrollView>
                <CalendarView
                    {...props}
                    handleMonthChange={handleMonthChange}
                    loader={loader}
                    availableDates={availableDates}
                    setNextDay={setNextDay}
                    nextDay={nextDay}
                />
                <View style={styles.actionContainer}>
                    <Text style={styles.headText}>Select Time</Text>
                    <View style={styles.timePickerContainer}>
                        <Picker
                            selectedValue={selectedValue}
                            style={{ width: '100%' }}
                            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                        >
                            <Picker.Item value="10:00 AM" label="10:00 AM" style={{ width: '100%' }} />
                            <Picker.Item value="11:00 AM" label="11:00 AM" style={{ width: '100%' }} />
                            <Picker.Item value="12:00 PM" label="12:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="01:00 PM" label="01:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="02:00 PM" label="02:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="03:00 PM" label="03:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="04:00 PM" label="04:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="05:00 PM" label="05:00 PM" style={{ width: '100%' }} />
                            <Picker.Item value="06:00 PM" label="06:00 PM" style={{ width: '100%' }} />
                        </Picker>
                    </View>
                    <Text style={styles.headText}>{selectedValue}</Text>
                    <View style={styles.timeSlotContainer}>
                        {timeSlot.map((item, index) => {
                            return <TouchableOpacity
                                style={activeSlot === index ? styles.activeTimeSlotButton : styles.timeSlotButton}
                                key={index}
                                onPress={() => handleTimeSlot(item, index)}
                                activeOpacity={0.7}>
                                <Text style={activeSlot === index ? styles.activeTime : styles.time}>{item.time}</Text>
                                {/* <View style={styles.badge}><Text style={{ color: "#fff", fontSize: 10 }}>3</Text></View> */}
                            </TouchableOpacity>
                        })}
                    </View>
                    <View style={styles.notify_container}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={toggleCheckBox}
                            onValueChange={(newValue) => setToggleCheckBox(newValue)}
                        />
                        <Text style={styles.notifyText}>Notify me</Text>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.actionButton}
                        // onPress={() => props.navigation.navigate('Booked')}
                        onPress={() => props.navigation.navigate('PreConfirmation', {
                            userData: userData,
                            hospital: hospital,
                            date: nextDay,
                            bookingTime: bookingTime
                        })}
                    >
                        <Text style={styles.buttonText}>BOOK</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                onClose={() => setToggleCheckBox(false)}
                onOpen={() => { }}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000038"
                    },
                    draggableIcon: {
                        backgroundColor: THEME_COLOR
                    }
                }}
            >
                <NotifyMe {...props} refRBSheet={refRBSheet} />
            </RBSheet>
        </View>
    )
}
export default AppointmentBook;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    timePickerContainer: {
        borderWidth: 1,
        borderRadius: 5,
        width: width / 2,
        borderColor: DARK_GRAY
    },
    headText: {
        fontSize: 18,
        marginVertical: 10,
        color: DARK_GRAY
    },
    actionContainer: {
        marginVertical: 10,
        paddingHorizontal: 15
    },
    timeSlotContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    timeSlotButton: {
        paddingHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: DARK_GRAY,
        width: (width - 80) / 3,
        height: 50,
        borderRadius: 5,
        paddingVertical: 5,
        marginBottom: 20
    },
    activeTimeSlotButton: {
        paddingHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: THEME_COLOR,
        width: (width - 80) / 3,
        height: 50,
        borderRadius: 5,
        paddingVertical: 5,
        marginBottom: 20,
        backgroundColor: THEME_COLOR
    },
    actionButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 15,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 20
    },
    buttonText: {
        fontSize: 16,
        color: '#fff',
    },
    activeTime: {
        color: '#fff'
    },
    time: {
        color: BLACK
    },
    badge: {
        position: "absolute",
        borderColor: "#fff",
        borderRadius: 10,
        top: -10,
        right: -6,
        width: 20,
        height: 20,
        borderWidth: 1,
        flex: 1,
        backgroundColor: THEME_COLOR,
        justifyContent: "center",
        alignItems: "center"
    },
    notify_container: {
        flexDirection: "row",
        alignItems: "center"
    },
    notifyText: {
        fontSize: 16
    }
})