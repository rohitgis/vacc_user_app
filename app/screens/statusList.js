import React from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions, TouchableOpacity } from 'react-native';
import { THEME_COLOR, DARK_GRAY } from "_config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { HeaderBackButton } from '_components';

const { width, height } = Dimensions.get("window");

const userData = [
    {
        name: "Rahul Singh",
        rel: "Employee"
    },
    {
        name: "Suraj Singh",
        rel: "Father"
    },
    {
        name: "Radhika Singh",
        rel: "Mother"
    }
]
const StatusList = (props) => {
    return (
        <View style={styles.container}>
            <HeaderBackButton {...props} name="Status" />
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollContainer}>
                {userData.map((item, index) => {
                    return <View key={index}><View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
                        <View style={{ flexDirection: 'row', alignItems: "center" }}>
                            <MaterialIcons name={item.rel === "Employee" ? "person" : "family-restroom"} size={25} color={THEME_COLOR} />
                            <View>
                                <Text style={styles.mainText}>{item.name}</Text>
                                <Text style={styles.subText}>{item.rel}</Text>
                            </View>
                        </View>
                        <TouchableOpacity activeOpacity={0.7} style={styles.actionButton} onPress={() => props.navigation.navigate("Status")}>
                            <Text>Details</Text>
                        </TouchableOpacity>
                    </View>
                        {index !== userData.length - 1 && <View style={styles.hr}></View>}</View>
                })}
            </ScrollView>
        </View>
    )
}
export default StatusList;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    mainText: {
        marginLeft: 5,
        fontSize: 16,
        // marginBottom: 10
    },
    subText: {
        marginLeft: 5,
        fontSize: 14,
        color: DARK_GRAY
    },
    normalText: {
        fontSize: 16,
        lineHeight: 22
    },
    actionButton: {
        borderWidth: 1,
        borderColor: THEME_COLOR,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
    },
    hr: {
        height: 1,
        width: "100%",
        backgroundColor: DARK_GRAY,
        marginVertical: 10
    }
})