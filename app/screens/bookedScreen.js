import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView } from "react-native";
import { THEME_COLOR, LIGHT_GRAY, DARK_GRAY, SUCCESS_COLOR } from "_config";
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import QRCode from 'react-native-qrcode-svg';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const BookedScreen = (props) => {
    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollViewStyle}>
                {/* <View style={styles.contentContainer}> */}
                {/* <View style={{ alignItems: 'flex-end', padding: 5 }}>
                    <Entypo name="cross" size={30} color={THEME_COLOR} />
                </View> */}
                {/* <View style={styles.mainBox}> */}

                <View style={{ alignItems: "center", marginVertical: 30 }}>
                    <MaterialIcons name="check-circle" size={40} color={SUCCESS_COLOR} />
                    <Text style={styles.confirmText}>Your vaccination appointment is confirmed</Text>
                    <Text style={styles.confirmTextSend}>Your appointment deails have also been sent on your registered mobile number and email addess.</Text>
                </View>
                <View style={styles.qrContainer}>
                    <QRCode
                        value="http://awesome.link.qr"
                        color={THEME_COLOR}
                        size={80}
                        enableLinearGradient={false}
                    />

                </View>
                <Text style={styles.bigText}>Stay Home, Stay Safe</Text>
                {/* <Text style={{ color: DARK_GRAY }}>You booked an appointment in</Text> */}
                {/* <Text>Muscat, May 29, 2021 at 10:10 AM</Text> */}
                <View style={{alignSelf: "flex-start", paddingHorizontal: 15}}>
                    <View style={styles.details}>
                        <Text style={styles.Heading}>Center</Text>
                        <Text>APOLLO HOSPITAL,</Text>
                        <Text>MUSCAT</Text>
                        <Text>MUSCAT, OMAN</Text>
                    </View>
                    <View style={styles.details}>
                        <Text style={styles.Heading}>Date and Time</Text>
                        <Text>06 JUN 2021, 10:10 AM</Text>
                    </View>
                </View>
                <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.actionButton}
                    onPress={() => props.navigation.navigate('Home')}
                >
                    <Text style={styles.buttonText}>GO TO HOME</Text>
                </TouchableOpacity>
                {/* </View> */}
                {/* </View> */}
            </ScrollView>
        </View>
    )
}
export default BookedScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    scrollViewStyle: {
        alignItems: "center",
        paddingVertical: 20
    },
    contentContainer: {
        width: width / 1.3,
        // height: height / 1.5,
        elevation: 4,
        backgroundColor: '#fff'
    },
    details: {
        marginBottom: 15
    },
    confirmText: {
        fontSize: 20,
        color: SUCCESS_COLOR,
        textAlign: "center",
        marginHorizontal: 20
    },
    Heading: {
        fontWeight: "bold",
        fontSize: 16
    },
    confirmTextSend: {
        fontSize: 18,
        marginTop: 10,
        textAlign: "center",
        marginHorizontal: 20
    },
    qrContainer: {
        backgroundColor: LIGHT_GRAY,
        borderRadius: 100,
        width: 130,
        height: 130,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mainBox: {
        alignItems: 'center'
    },
    bigText: {
        paddingVertical: 30,
        fontSize: 20,
        fontWeight: 'bold'
    },
    buttonText: {
        color: '#fff',
        fontSize: 16
    },
    actionButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 15,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 50,
        width: '90%',
        marginBottom: 20
    }
})