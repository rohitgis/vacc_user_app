import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, TextInput, Dimensions, TouchableOpacity, ImageBackground, ToastAndroid, AsyncStorage } from 'react-native';
import { THEME_COLOR, DARK_GRAY, BLACK } from "_config";
import { userCheck } from "_services/userService";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const LoginScreen = (props) => {
    const [idNumber, setIdNumber] = useState("");
    const [mobileNo, setMobileNo] = useState("");

    const handleGetOtp = async () => {
        // if (!idNumber) {
        //     ToastAndroid.show("Please enter Id number", ToastAndroid.SHORT);
        // } else if (!mobileNo) {
        //     ToastAndroid.show("Please enter mobile number", ToastAndroid.SHORT);
        // } else if (idNumber && mobileNo) {
        //     try {
        //         let result = await userCheck(mobileNo, idNumber);
        //         console.log(result, "user check")
        //         if (result.message === "user available" || result.message === "fresh user") {
                    const otp = Math.random();
                    var generated = `${Math.ceil(otp * 1000000)}`
                    while (generated.length < 6) {
                        generated = '0' + generated
                    }
                    console.log(generated)
        //             if (result.message === "user available") {
        //                await AsyncStorage.setItem("userId", result.id)
        //             }
                    props.navigation.navigate('Verification', {
                        otp: generated,
                        idNumber: idNumber,
                        mobileNo: mobileNo,
                        // check: result
                    })
        //         } else if (result.message === "user available but not proper combination") {
        //             ToastAndroid.show("You are not registered by your company so please contact your company", ToastAndroid.SHORT);
        //         }
        //     } catch (error) {
        //         console.log(error)
        //     }

        // }
    }
    return <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={{ justifyContent: 'space-evenly', alignItems: 'center', height: height }}>
                <ImageBackground source={require('_assets/Login-Design-01.png')} style={styles.BGImage}>
                    <Image source={require('_assets/Logo.png')} style={styles.logoStyle} />
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <TextInput
                                style={styles.inputField}
                                placeholder="National ID"
                                value={idNumber}
                                onChangeText={(value) => setIdNumber(value)}
                            />
                        </View>
                        {/* <View style={styles.inputContainer}>
                            <TextInput
                                style={styles.inputField}
                                keyboardType="phone-pad"
                                placeholder="Mobile Number"
                                value={mobileNo}
                                onChangeText={(value) => setMobileNo(value)}
                            />
                        </View> */}
                    </View>
                    <View style={styles.actionContainer}>
                        <Text style={styles.descText}>We will send you a <Text style={styles.highlightText}>One Time Password</Text> on your phone number</Text>
                        <TouchableOpacity
                            onPress={handleGetOtp}
                            activeOpacity={0.7}
                            style={styles.actionButton}>
                            <Text style={styles.buttonText}>GET OTP</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>

        </ScrollView>
    </View>
}
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 30,
        // paddingVertical: 30,
        alignItems: 'center'
    },
    logoStyle: {
        width: 120,
        height: 59
    },
    inputContainer: {
        borderWidth: 1,
        borderColor: DARK_GRAY,
        width: width - 60,
        marginBottom: 20,
        paddingHorizontal: 10
    },
    formContainer: {
        // marginVertical: 50
    },
    inputField: {
        fontSize: 16
    },
    actionContainer: {
        // marginTop: 30
    },
    descText: {
        fontSize: 18,
        color: DARK_GRAY,
        textAlign: 'center',
        lineHeight: 22
    },
    highlightText: {
        color: BLACK
    },
    actionButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 15,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 20
    },
    buttonText: {
        fontSize: 16,
        color: '#fff',
    },
    BGImage: {
        flex: 1,
        width: width,
        resizeMode: "cover",
        justifyContent: "space-evenly",
        paddingHorizontal: 30,
        alignItems: 'center'
    }
})