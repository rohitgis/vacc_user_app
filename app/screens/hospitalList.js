import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, StyleSheet, TouchableOpacity, Platform, PermissionsAndroid, ScrollView } from "react-native";
import { HeaderBackButton } from "_components";
import Geolocation from '@react-native-community/geolocation';
import { LIGHT_GRAY, DARK_GRAY, THEME_COLOR_LIGHT } from "_config";
import { getNearByHospitals } from "_services/hospitalService";

const width = Dimensions.get('window').width;

const hospitals = [
    {
        name: "Apollo Hospital",
        address: "Muscat",
        city: "Muscat, Oman"
    },
    {
        name: "Sar Sundar Lal Hospital",
        address: "Muscat",
        city: "Muscat, Oman"
    }
]

const HospitalList = (props) => {
    const { userData } = props.route.params;
    const [currentLocation, setCurrentLocation] = useState(null)
    const [naerbyHospitals, setNearbyHospitals] = useState([]);
    useEffect(() => {
        requestLocationPermission();
    }, [])
    useEffect(() => {
        getNearByHospitalsList()
    }, [currentLocation])
    const getNearByHospitalsList = async () => {
        try {
            let result = await getNearByHospitals();
            if (result.length > 0) {
                setNearbyHospitals(result)
            }
        } catch (error) {
            console.log(error)
        }
    }
    const requestLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            getOneTimeLocation();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: 'Location Access Required',
                        message: 'This App needs to Access your location',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    getOneTimeLocation();
                } else {
                    console.log('Permission Denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };
    const getOneTimeLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                setCurrentLocation(position.coords)
                console.log(position.coords)
            },
            (error) => {
                console.log(error);
            },
            {
                enableHighAccuracy: true,
                timeout: 30000,
                maximumAge: 1000
            },
        );
    };
    const handleHospitalSelect = (item) => {
        props.navigation.navigate('appointmentBook', {
            userData: userData,
            hospital: item
        })
    }
    return (
        <View style={styles.container}>
            <HeaderBackButton {...props} name="Select One Hospital" />
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollContainer}>
                {/* {naerbyHospitals.map((item, index) => {
                    return  */}
                {hospitals.map((item, index) => {
                    return <TouchableOpacity
                        key={index}
                        activeOpacity={0.7}
                        style={styles.Buttons}
                        onPress={() => handleHospitalSelect(item)}
                    >
                        <Text style={styles.buttonText}>{item.name}</Text>
                        <View style={{ marginTop: 10 }}>
                            <Text style={styles.buttonTextNormal}>{item.address}</Text>
                            <Text style={styles.buttonTextNormal}>{item.city}</Text>
                        </View>
                    </TouchableOpacity>
                })}
                {/* <TouchableOpacity
                    // key={index}
                    activeOpacity={0.7}
                    style={styles.Buttons}
                    onPress={() => handleHospitalSelect()}
                >
                    <Text style={styles.buttonText}>Sar Sundar Lal Hospital</Text>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.buttonTextNormal}>Oman</Text>
                        <Text style={styles.buttonTextNormal}>Muscat, Oman</Text>
                    </View>
                </TouchableOpacity> */}
                {/* })} */}
            </ScrollView>
        </View>
    )
}
export default HospitalList;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    Buttons: {
        backgroundColor: THEME_COLOR_LIGHT,
        borderRadius: 5,
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 5
    },
    buttonText: {
        fontSize: 17
    },
    buttonTextNormal: {
        fontSize: 14,
        color: DARK_GRAY
    }
})