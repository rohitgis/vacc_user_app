import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Linking } from "react-native";
import { HeaderBackButton } from "_components";
import { THEME_COLOR, DARK_GRAY } from "_config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Calender_icon, Time_icon, Address_icon, Call_icon } from "_icons";
import QRCode from 'react-native-qrcode-svg';

const StatusScreen = (props) => {
    const handleLinkingMap = () => {
        // Linking.openURL("https://goo.gl/GW6uOo")
        Linking.openURL('google.navigation:q=100+101')
    }
    return (
        <View style={styles.container}>
            <HeaderBackButton {...props} name="Appointment Details" />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.mainContainer}>
                    <View style={styles.mainSection}>
                        <Text style={styles.headText}>Your Vaccination Status</Text>
                        <View style={styles.bottomSection}>
                            <View style={styles.detailsSection}>
                                <View style={styles.row}>
                                    <Calender_icon width={20} height={20} />
                                    <Text style={styles.normalText}>Jun 06, 2021</Text>
                                </View>
                                <View style={styles.row}>
                                    <Time_icon width={20} height={20} />
                                    <Text style={styles.normalText}>10:10 AM</Text>
                                </View>
                                <View style={styles.row}>
                                    <Address_icon width={20} height={20} />
                                    <Text style={styles.normalText}>Muscat</Text>
                                </View>
                                <View style={styles.row}>
                                    <Call_icon width={20} height={20} />
                                    <Text style={styles.normalText}>+968 1234 5678</Text>
                                </View>
                                <View>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        style={styles.rescheduleButton}
                                    >
                                        <Text style={styles.buttonText}>CANCEL</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={handleLinkingMap}
                                        style={styles.rescheduleButton}
                                    >
                                        <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                            <MaterialIcons name="navigation" color="#fff" size={18} />
                                            <Text style={styles.buttonText}>NAVIGATE</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <QRCode
                                    value="http://awesome.link.qr"
                                    color={THEME_COLOR}
                                    size={100}
                                    enableLinearGradient={false}
                                />
                            </View>
                        </View>
                    </View>
                    {/* <View style={styles.notificationContainer}>
                        <View style={{ alignItems: 'flex-end', padding: 10 }}>
                            <Entypo name="cross" size={20} color={THEME_COLOR} />
                        </View>
                        <View style={{ paddingHorizontal: 20, height: 60, justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Your Appointment is....</Text>
                            <Text style={{ color: DARK_GRAY }}>5 minutes ago</Text>
                        </View>
                    </View>
                    <View style={styles.notificationContainer}>
                        <View style={{ alignItems: 'flex-end', padding: 10 }}>
                            <Entypo name="cross" size={20} color={THEME_COLOR} />
                        </View>
                        <View style={{ paddingHorizontal: 20, height: 60, justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Your Appointment is....</Text>
                            <Text style={{ color: DARK_GRAY }}>5 minutes ago</Text>
                        </View>
                    </View>
                    <View style={styles.notificationContainer}>
                        <View style={{ alignItems: 'flex-end', padding: 10 }}>
                            <Entypo name="cross" size={20} color={THEME_COLOR} />
                        </View>
                        <View style={{ paddingHorizontal: 20, height: 60, justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 16 }}>Your Appointment is....</Text>
                            <Text style={{ color: DARK_GRAY }}>5 minutes ago</Text>
                        </View>
                    </View> */}
                </View>
            </ScrollView>
        </View>
    )
}
export default StatusScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    mainContainer: {
        padding: 20
    },
    mainSection: {
        backgroundColor: THEME_COLOR,
        padding: 20,
        borderRadius: 5
    },
    headText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    bottomSection: {
        flexDirection: 'row',
        paddingVertical: 10,
        justifyContent: 'space-between'
    },
    row: {
        flexDirection: 'row',
        paddingVertical: 10
    },
    normalText: {
        color: '#fff',
        fontSize: 16,
        paddingLeft: 5
    },
    detailsSection: {
        // paddingVertical: 10
    },
    buttonText: {
        color: '#fff',
        fontSize: 16
    },
    rescheduleButton: {
        borderWidth: 1,
        borderRadius: 5,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#fff',
        marginTop: 20,
        width: '100%'
    },
    notificationContainer: {
        borderWidth: 1,
        borderColor: DARK_GRAY,
        borderRadius: 5,
        paddingBottom: 15,
        height: 120,
        marginVertical: 20,
        flexDirection: 'column'
        // justifyContent: 'space-between',
        // borderWidth: 0

    }
})