import React from "react";
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
import { THEME_COLOR } from "_config";

const ProfileScreen = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity activeOpacity={0.7} style={styles.actionButton}>
                <Text style={styles.buttonText}>Log Out</Text>
            </TouchableOpacity>
        </View>
    )
}
export default ProfileScreen();

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    actionButton: {
        borderRadius: 5,
        backgroundColor: THEME_COLOR,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        
    }
})