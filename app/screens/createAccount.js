import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, TextInput, Dimensions, TouchableOpacity, ToastAndroid } from 'react-native';
import { THEME_COLOR, DARK_GRAY } from "_config";
import { HeaderBackButton } from "_components";
import { Create_account_icon } from "_icons";
import { createUser } from "_services/userService";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const CreateAccount = (props) => {
    const { idNumber, mobileNo } = props.route.params;
    const [name, setName] = useState("");
    const [dob, setDob] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [district, setDistrict] = useState("");
    const [state, setState] = useState("");
    const [postBox, setPostBox] = useState("");

    const handleSignUp = async () => {
        let errorList = [];
        if (!name) {
            errorList.push('name')
            ToastAndroid.show("Name can not be empty", ToastAndroid.SHORT);
        } else if (!dob) {
            errorList.push('dob')
            ToastAndroid.show("DOB can not be empty", ToastAndroid.SHORT);
        } else if (!email) {
            errorList.push('email')
            ToastAndroid.show("Email can not be empty", ToastAndroid.SHORT);
        } else if (!address) {
            errorList.push('address')
            ToastAndroid.show("Address can not be empty", ToastAndroid.SHORT);
        } else if (!city) {
            errorList.push('city')
            ToastAndroid.show("City can not be empty", ToastAndroid.SHORT);
        } else if (!district) {
            errorList.push('district')
            ToastAndroid.show("District can not be empty", ToastAndroid.SHORT);
        } else if (!state) {
            errorList.push('state')
            ToastAndroid.show("State can not be empty", ToastAndroid.SHORT);
        } else if (!postBox) {
            errorList.push('postBox')
            ToastAndroid.show("Post Box can not be empty", ToastAndroid.SHORT);
        }
        if (errorList.length === 0) {
            try {
                let result = await createUser(idNumber, mobileNo, 'aadhar card', name, email, dob, address, city, district, state, postBox, "")
                if (result.success === true) {
                    props.navigation.replace('Home')
                }
            } catch (error) {
                console.log(error)
            }
        }
    }
    return <View style={styles.container}>
        <HeaderBackButton {...props} />
        <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={styles.mainContainer}>
                <View style={styles.topContainer}>
                    <Create_account_icon width={120} height={120} />
                    <Text style={styles.bigText}>Create Account</Text>
                    <Text style={styles.mediumText}>Tell us your personal details please</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="Name"
                            value={name}
                            onChangeText={value => setName(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="DOB"
                            value={dob}
                            onChangeText={value => setDob(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="Email"
                            keyboardType="email-address"
                            value={email}
                            onChangeText={value => setEmail(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="Address"
                            value={address}
                            onChangeText={value => setAddress(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="City"
                            value={city}
                            onChangeText={value => setCity(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="District"
                            value={district}
                            onChangeText={value => setDistrict(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="State"
                            value={state}
                            onChangeText={value => setState(value)}
                        />
                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="Post Box"
                            value={postBox}
                            onChangeText={value => setPostBox(value)}
                        />
                    </View>
                </View>
                <View >
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, color: DARK_GRAY, textAlign: 'center' }}>Select your city and hospital as per your convenience</Text>
                    </View>
                    <TouchableOpacity
                        style={styles.actionButton}
                        activeOpacity={0.7}
                        onPress={handleSignUp}
                    >
                        <Text style={styles.buttonText}>CREATE ACCOUNT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    </View>
}
export default CreateAccount;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 30,
        // paddingVertical: 30,
        alignItems: 'center'
    },
    topContainer: {
        alignItems: 'center'
    },
    bigText: {
        fontSize: 30,
        marginBottom: 30
    },
    mediumText: {
        fontSize: 16,
    },
    inputContainer: {
        borderWidth: 1,
        borderColor: DARK_GRAY,
        width: width - 60,
        // marginBottom: 20,
        paddingHorizontal: 10,
        marginVertical: 10
    },
    inputField: {
        fontSize: 16,
        // textAlign: 'center',
    },
    mainContainer: {
        // height: height - 50,
        justifyContent: 'space-evenly'
    },
    actionButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 15,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 20,
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        color: '#fff',
    }
})