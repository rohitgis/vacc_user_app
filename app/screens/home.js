import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, Dimensions, ImageBackground, TouchableOpacity } from 'react-native';
import { THEME_COLOR, DARK_GRAY } from "_config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { getUserBookingDetail } from "_services/userService";


const userData = [
    {
        name: "Rahul Singh",
        rel: "Employee"
    },
    {
        name: "Suraj Singh",
        rel: "Father"
    },
    {
        name: "Radhika Singh",
        rel: "Mother"
    }
]

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const HomeScreen = (props) => {
    const [userBookingStatus, setUserBooingStatus] = useState(null);

    useEffect(() => {
        getUserBookingStatus();

    }, [])

    const handleBookApointment = (item) => {
        props.navigation.navigate("HospitalList", {
            userData: item
        })
    }
    const getUserBookingStatus = async () => {
        try {
            let res = await getUserBookingDetail();
            setUserBooingStatus(res)
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scrollStyle}>
                <View style={{ alignItems: 'center', height: height }}>
                    <ImageBackground source={require('_assets/Login-Design-01.png')} style={styles.BGImage}>
                        <View style={styles.topView}>
                            <Image source={require('_assets/Logo.png')} style={styles.logoImage} />
                        </View>
                        <View style={styles.smartContainer}>
                            {userData.map((item, index) => {
                                return <View key={index}><View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
                                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                        <MaterialIcons name={item.rel === "Employee" ? "person" : "family-restroom"} size={25} color={THEME_COLOR} />
                                        <View>
                                            <Text style={styles.mainText}>{item.name}</Text>
                                            <Text style={styles.subText}>{item.rel}</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity activeOpacity={0.7} style={styles.actionButton} onPress={() => handleBookApointment(item)}>
                                        <Text>Book</Text>
                                    </TouchableOpacity>
                                </View>
                                    {index !== userData.length - 1 && <View style={styles.hr}></View>}</View>
                            })}
                        </View>
                    </ImageBackground>
                </View>
            </ScrollView>
        </View>
    )
}
export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topView: {
        alignItems: 'center',
        marginVertical: 30
    },
    BGImage: {
        flex: 1,
        width: width,
        resizeMode: "cover",
        // justifyContent: "space-evenly",
        paddingHorizontal: 15,
        // alignItems: 'center'
    },
    logoImage: {
        width: 120,
        height: 59,
        marginTop: 80
    },
    smartContainer: {
        borderColor: THEME_COLOR,
        borderWidth: 1,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    scrollStyle: {
        paddingHorizontal: 15,
        width: width
    },
    mainText: {
        marginLeft: 5,
        fontSize: 16,
        // marginBottom: 10
    },
    subText: {
        marginLeft: 5,
        fontSize: 14,
        color: DARK_GRAY
    },
    normalText: {
        fontSize: 16,
        lineHeight: 22
    },
    actionButton: {
        borderWidth: 1,
        borderColor: THEME_COLOR,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
    },
    hr: {
        height: 1,
        width: "100%",
        backgroundColor: DARK_GRAY,
        marginVertical: 10
    }
})