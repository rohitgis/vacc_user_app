import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, Dimensions, TouchableOpacity, ToastAndroid, AsyncStorage } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { DARK_GRAY, THEME_COLOR } from "_config";
import { Verification_icon } from "_icons";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const LoginVerification = (props) => {
    const { otp, idNumber, mobileNo } = props.route.params;
    const [otpInput, setOtpInput] = useState("");
    const [toggleCheckBox, setToggleCheckBox] = useState(false);

    const handleOtpVerify = () => {
        // if (otp === otpInput) {
        //     if (check.message === "user available") {
        props.navigation.replace('Home')
        //     } else if (check.message === "fresh user") {
        // props.navigation.navigate('signUp', {
        //     idNumber: idNumber,
        //     mobileNo: mobileNo
        // })
        // }

        // } else {
        //     ToastAndroid.show("Incorrect OTP", ToastAndroid.SHORT);
        // }
    }
    return <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={styles.mainContainer}>
                <View style={styles.topContainer}>
                    <Verification_icon width={120} height={120} />
                    <Text style={styles.bigText}>Verification Code</Text>
                    <Text style={styles.mediumText}>SMS with code has been send to</Text>
                    <Text style={styles.mediumText}>*******910</Text>
                    {/* <Text>{`Your otp: ${otp}`}</Text> */}
                </View>
                <View style={styles.middleContainer}>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.inputField}
                            placeholder="OTP"
                            secureTextEntry
                            value={otpInput}
                            onChangeText={value => setOtpInput(value)}
                        />
                    </View>
                    <View style={styles.checkboxContainer}>
                        <CheckBox
                            tintColors={{ true: THEME_COLOR, false: DARK_GRAY }}
                            value={toggleCheckBox}
                            onValueChange={(newValue) => setToggleCheckBox(newValue)}
                        />
                        <Text>I hereby confirm that I wish to be vaccinated against COVID-19</Text>
                    </View>
                </View>
                <View >
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, color: DARK_GRAY }}>Din’t receive the verification OTP?</Text>
                        <Text style={{ fontSize: 18 }}>Resend again</Text>
                    </View>
                    <TouchableOpacity
                        style={styles.actionButton}
                        activeOpacity={0.7}
                        onPress={handleOtpVerify}
                    >
                        <Text style={styles.buttonText}>Verify</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    </View>
}
export default LoginVerification;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollContainer: {
        paddingHorizontal: 30,
        // paddingVertical: 30,
        alignItems: 'center'
    },
    topContainer: {
        alignItems: 'center'
    },
    bigText: {
        fontSize: 30,
        marginBottom: 30
    },
    mediumText: {
        fontSize: 16,
    },
    inputContainer: {
        borderWidth: 1,
        borderColor: DARK_GRAY,
        width: width - 60,
        // marginBottom: 20,
        paddingHorizontal: 10,
        // marginVertical: 80
    },
    inputField: {
        fontSize: 16,
        textAlign: 'center',
    },
    mainContainer: {
        height: height - 50,
        justifyContent: 'space-evenly'
    },
    actionButton: {
        backgroundColor: THEME_COLOR,
        paddingVertical: 15,
        alignItems: 'center',
        borderRadius: 5,
        marginTop: 20
    },
    buttonText: {
        fontSize: 16,
        color: '#fff',
    },
    checkboxContainer: {
        flexDirection: "row",
        marginTop: 10
    },
    middleContainer: {
        alignItems: "center"
    }
})