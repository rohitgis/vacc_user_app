import React from 'react';
import { View, StyleSheet } from 'react-native';
import RootNavigator from "_navigator";

const App = () => {
    return (
        <View style={styles.container}>
            <RootNavigator />
        </View>
    );
}
export default App;
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})